# # Stage 1: Compile and Build angular codebase

# # Use official node image as the base image
# FROM node:latest as build

# # Set the working directory
# WORKDIR /usr/local/app

# # Add the source code to app
# COPY ./ /usr/local/app/

# # Install all the dependencies
# RUN npm install

# # Generate the build of the application
# RUN npm run build

# #Stage 2
# #######################################
# #pull the official nginx:1.19.0 base image
# FROM nginx:1.19.0
# #copies React to the container directory
# # Set working directory to nginx resources directory
# WORKDIR /usr/share/nginx/html
# # Remove default nginx static resources
# RUN rm -rf ./*
# # Copies static resources from builder stage
# COPY --from=build /usr/local/app/dist .
# # Containers run nginx with global directives and daemon off
# ENTRYPOINT ["nginx", "-g", "daemon off;"]





# # pull official base image
# FROM node:16 AS builder

# # set working directory
# WORKDIR /app


# # install app dependencies
# #copies package.json and package-lock.json to Docker environment
# COPY ./ ./

# # Installs all node packages
# RUN npm install 

# ENV API_URL: "http://localhost:8080"

# # Copies everything over to Docker environment
# # COPY . ./
# RUN npm run build

# #Stage 2
# #######################################
# #pull the official nginx:1.19.0 base image
# FROM nginx:1.19.0
# #copies React to the container directory
# # Set working directory to nginx resources directory
# WORKDIR /usr/share/nginx/html
# # Remove default nginx static resources
# RUN rm -rf ./*
# # Copies static resources from builder stage
# COPY --from=builder /app/dist .
# # Containers run nginx with global directives and daemon off
# ENTRYPOINT ["nginx", "-g", "daemon off;"]





# FROM node:16 as node
# WORKDIR /app
# COPY . .
# RUN npm install -g @angular/cli
# RUN npm install
# ENV API_URL: "http://localhost:8080"
# RUN ng build

# #stage 2
# # FROM nginx:1-alpine
# # COPY --from=node /app/dist/mds-dds-exam-angular /usr/share/nginx/html
# # ENTRYPOINT ["nginx", "-g", "daemon off;"]

# #Stage 2
# #######################################
# #pull the official nginx:1.19.0 base image
# FROM nginx:1.19.0
# # Set working directory to nginx resources directory
# WORKDIR /usr/share/nginx/html
# # Remove default nginx static resources
# RUN rm -rf ./*
# # Copies static resources from builder stage
# COPY --from=node /app/dist/mds-dds-exam-angular .
# # Containers run nginx with global directives and daemon off
# # ENTRYPOINT ["nginx", "-g", "daemon off;"]

FROM node:16 as node
WORKDIR /app
COPY . .
RUN npm install -g @angular/cli
RUN npm install
ENV API_URL: "http://localhost:8080"
RUN ng build

FROM nginx
COPY nginx/default.conf /etc/nginx/conf.d
COPY dist/mds-dds-exam-angular /usr/share/nginx/html